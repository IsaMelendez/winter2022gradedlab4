import java.util.Scanner;

public class ApplianceStore
{
	public static void main(String[] args)
	{
		Oven[] myOvens = new Oven[4];
		
		Scanner input = new Scanner(System.in);
		
		for(int i = 0; i<myOvens.length; i++)
		{
			/*This program assumes that the user enters the correct 
			input (i.e. ints when prompted for a number, strings when
			prompted for a string, etc)*/
			
			myOvens[i] = new Oven();
			
			System.out.print("What temperature is oven number " + (i + 1) + " at currently? ");
			int temp1 = input.nextInt();
			
			System.out.print("What temperature should oven number " + (i + 1) + " reach? ");
			int temp2 = input.nextInt();
			
			System.out.print("Is the light in oven number " + (i + 1) + " on? ");
			String lightOn = input.next();
			lightOn = lightOn.toLowerCase();
			
			while(!lightOn.equals("yes") && !lightOn.equals("no"))
			{
				System.out.println("Error: You must answer with yes or no. ");
				lightOn = input.next();
				lightOn = lightOn.toLowerCase();
			}
			
			boolean light;
			
			if(lightOn.equals("yes"))
			{
				light = true;
			}
			else
			{
				light = false;
			}
			
			System.out.print("What food are you cooking in oven number " + (i + 1) + "? ");
			String food;
			food = input.next();
			
			myOvens[i].initialize(light, temp1, temp2, food);
			
			System.out.println();
		}
		
		System.out.println("Input what food you want to put in the 2nd oven");
		String food2 = input.next();
		myOvens[1].changeFood(food2);
		
		System.out.println();
		//To separate the results.
		
		System.out.println("Current temperature for the 4th oven: " + myOvens[3].getCurTemp());
		System.out.println("Input new current temperature: ");
		int newCTemp = input.nextInt();
		
		System.out.println("Target temperature for the 4th oven: " + myOvens[3].getTarTemp());
		System.out.println("Input new target temperature: ");
		int newTTemp = input.nextInt();
		
		System.out.println("The light in the 4th oven is on: " + myOvens[3].getLight());
		System.out.println("You cannot turn the light off/on. There is no setter method for this attribute.");
		
		System.out.println("Food inside the 4th oven: " + myOvens[3].getFood());
		System.out.println("Input a new food for oven 4: ");
		String newFood = input.next();
		
		myOvens[3].setTarTemp(newTTemp);
		myOvens[3].setCurTemp(newCTemp);
		myOvens[3].setFood(newFood);
		
		System.out.println();
		
		System.out.println("Current temperature for the 4th oven: " + myOvens[3].getCurTemp());
		System.out.println("Target temperature for the 4th oven: " + myOvens[3].getTarTemp());
		System.out.println("The light in the 4th oven is on: " + myOvens[3].getLight());
		System.out.println("Food inside the 4th oven: " + myOvens[3].getFood());
		
		myOvens[0].raiseTemp();
		myOvens[0].pressLightButton();
		myOvens[0].printFoodReady();
		
	}
}