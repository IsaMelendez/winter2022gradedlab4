import java.util.Scanner;

public class Oven
{
	private boolean lightIsOn;
	private int currentTemp;
	private int targetTemp;
	private String food;
	
	//-------CONSTRUCTOR METHOD 
	
	public void initialize(boolean lightInput, int tempInput1, int tempInput2, String foodInput)
	{
		this.lightIsOn = lightInput;
		this.currentTemp = tempInput1;
		this.targetTemp = tempInput2;
		this.food = foodInput;
	}
	
	//-------GET AND SET LIGHT
	
	public boolean getLight()
	{
		return this.lightIsOn;
	}
	/*
	public void setLight(boolean input)
	{
		this.lightIsOn=input;
	}
	*/
	
	//-------GET AND SET CURRENT TEMP
	
	public int getCurTemp()
	{
		return this.currentTemp;
	}
	public void setCurTemp(int input)
	{
		this.currentTemp = input;
	}
	
	//-------GET AND SET TARGET TEMP
	
	
	public int getTarTemp()
	{
		return this.targetTemp;
	}
	public void setTarTemp(int input)
	{
		this.targetTemp = input;
	}
	
	//-------GET AND SET FOOD
	
	public String getFood()
	{
		return this.food;
	}
	public void setFood(String input)
	{
		this.food = input;
	}
	//---------------------------METHODS
	
	public void raiseTemp()
	{
		/*For the sake of simplicity, this program will assume that the 
		oven's temperature can increase immediately.*/
		
		System.out.println("Temperature has gone from " + this.currentTemp + " to " + this.targetTemp);
		this.currentTemp = targetTemp;
	}
	
	public void pressLightButton()
	{
		if(this.lightIsOn)
		{
			System.out.println("The light is currently on. You can see your " + this.food + " inside the oven.");
			this.lightIsOn = false;
		}
		else
		{
			System.out.println("The light is currently off.");
			this.lightIsOn = true;
		}
	}
	
	public void printFoodReady()
	{
		System.out.println("Your " + this.food + " is ready!");
	}
	
	public void changeFood(String newFood)
	{
		this.food = validateFood(newFood);
	}
	private String validateFood(String foodInput)
	{
		Scanner input = new Scanner(System.in);
		while(foodInput.equals("IceCream") || foodInput.equals("Milk"))
		{
			System.out.println("Please input another kind of food");
			foodInput = input.next();
		}
		return foodInput;
	}
}